var profile = new Vue({
  el:"#profile_updatable",
  data: {
    sidePanelToggle: true,
    avatar: '',
    username: 'DemoUser1',
    progress: 0,
    climbs: [],
    badges: [],
    timespent: '0',
  },
  methods: {
    togglePanel: function(e) {

      let element = document.getElementById('side_planel_id');
      console.log(element);
      if(element) {

        if(this.sidePanelToggle) {
          element.style.left = '-45%';
        } else {
          element.style.left = '0';
        }
        this.sidePanelToggle = !this.sidePanelToggle;
      }
    }
  }
});
