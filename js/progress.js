
function toggleOnSidePanel() {

}

var progress = new Vue({
  el: "#progress",
  data: {
    sidePanelToggle: false,
    currentFocus: null,
    context: {
      title: '',
      content: '',
    },
    component: {
      title: 'Fundamentals',
      sub: 'Progress'
    },
    topics: [
      { name: 'Hello World', index: 0, collapsed: true, hasFocus: false, isCompleted: true, tasks:
        [
          { name: "Welcome!", isCompleted: false, hasFocus: false, type:"reading", content:[
            'Welcome! Thank you for stopping by!',
            'Instead of boring you with a bunch of detailed information we are going to just briefly discuss how this works and jump straight into making our first program',
            'Climbs - This is where you embark on a new journey, encountering new obstacles that will build off experience from old ones',
            'Readings - Readings will help guide you and understand the different concepts within javascript.',
            'Practice - Nothing beats learning to program than actually programming. These will open up into a text editor and provide a small problem for you to solve and verify if you have solved it',
            'Quiz - On top of practice, you will be quizzed to ensure you understand the concepts you are covering. In this case, '
          ], parent: 0, index: 0 },
          { name: "Practice", isCompleted: false, hasFocus: false, type:"practice", url:"editor.html", parent: 0, index: 1 },
          { name: "Quiz", isCompleted: false, hasFocus: false, type:"quiz", url:"quiz.html", parent: 0, index: 2 },
        ]
      },
      { name: 'Variables', index: 1, collapsed: true, hasFocus: false, isCompleted: false, tasks:
        [
          { name: "Storing values", isCompleted: false, hasFocus: false, type:"reading", content:[
            'We need some way of storing data and this is where we will introduce variables. We can store variables by using the var or let keyword, both have keywords have a very different meaning that we will discuss shortly.',
            'However, to simply get started',
            'to declare a variable we use the var keyword in conjunction with a name for that variable',
            'Like so: var x;',
            'However this has created a variable that is not initialised, to initialise it we will use the = operator (assignment)',
            'var x = 5;',
            'This now stores the value 5 in the variable x.',
            'When we want to use this variable, we can output the value like so: ',
            'console.log(x);',
            'This will output the value that x is to the console.'
          ], parent: 1, index: 0 },
          { name: "Practice", isCompleted: false, hasFocus: false, type:"practice", url:"editor_var.html", parent: 1, index: 1 },
          { name: "Quiz", isCompleted: false, hasFocus: false, type:"quiz", url:"quiz_var.html", parent: 1, index: 2 },
        ]
      },
      { name: 'Operators', index: 2, collapsed: true, hasFocus: false, isCompleted: false, tasks:
        [
          { name: "Symbol soup!", isCompleted: false, hasFocus: false, type:"reading", content:[
            'There is always more than one way to perform something and there is a bunch of primitive methods o f',
            'Some basic mathematical :',
            'Adding two numbers: var x = 5 + 5;',
            'var y = 2; var x = 5 + y; We can also add the values of variables together.'
          ], parent: 2, index: 0 },
          { name: "Practice", isCompleted: false, hasFocus: false, type:"practice", url:"editor_exp.html", parent: 2, index: 1 },
          { name: "Quiz", isCompleted: false, hasFocus: false, type:"quiz", url:"quiz_exp.html", parent: 2, index: 2 },
        ]
      },
      { name: 'Loops (For)', index: 3, collapsed: true, hasFocus: false, isCompleted: false, tasks:
        [
          { name: "How they work", isCompleted: false, hasFocus: false, type:"reading",
          content:['Eventually we will compute data that can be variably length but not only would we want to iterate through' +
          'arrays we would want to also be able to iterate through objects or associative arrays\n\n.', 'We are not limited to just numerical ranges' +
          ', we can iterate through sets of values that may have different kind of order.'],
          parent: 3, index: 0 },
          { name: "Practice", isCompleted: false, hasFocus: false, type:"practice", url:"editor_for.html", parent: 0, index: 1 },
          { name: "Quiz", isCompleted: false, hasFocus: false, type:"quiz", url:"quiz_for.html", parent: 3, index: 1 },
        ]

      },
    ]
  },
  methods: {

    toggleTopic: function(e) {
      if(e.target.dataset['parent'] === '-1' || !e.target.dataset['parent']) {
        let n = Number.parseInt(e.target.dataset['index']);
        if(this.currentFocus != this.topics[n]) {
          if(this.currentFocus != null) {
            this.currentFocus.hasFocus = false;
          }
          this.currentFocus = this.topics[n];
          this.currentFocus.hasFocus = true;
          this.currentFocus.collapsed = false;
        } else {
          this.currentFocus.collapsed = true;
          this.currentFocus.hasFocus = false;
          this.currentFocus = null;
        }
      }
    },
    openReading: function(e) {
      let p = Number.parseInt(e.target.dataset['parent']);
      let i = Number.parseInt(e.target.dataset['index']);
      this.context.title = this.topics[p].tasks[i].name;
      this.context.content = this.topics[p].tasks[i].content;
    },
    togglePanel: function(e) {
      let element = document.getElementById('side_planel_mobile');
      if(element) {
        console.log(element);
        if(this.sidePanelToggle) {
          element.style.left = '-45%';
        } else {
          element.style.left = '0';
        }
        this.sidePanelToggle = !this.sidePanelToggle;
      }
    }
  }
});
