var v = new Vue({
  el: "#quiz",
  data: {
    toConfirm: false,
    goNext: false,
    correct_answers: 0,
    current_question_index: 0,
    current_selected: -1,
    current_question: { text: "var x; var y = 10;, Is this valid?"},
    current_answers: [
      {text: "True", letter: "A", index: 0, lockedIn: false},
      {text: "False", letter: "B", index: 1, lockedIn: false },
    ],
    quiz: {
      questions: [
        { text: "var x; var y = 10;, Is this valid?"},
        { text: "var x = true; var y = 10; var z = (y > 10) || (x); What is the value of z?"},
        { text: "var x = (1 < 10); y = !(!x)"},
      ],
      explanations: [
        "Yes, this is a declaration and initialisation",
        "z is true because x is true",
        "y is true because the expression of x evaluates to true (because 1 is < 10) and the ! operator is applied twice and therefore cancel each out",
      ],
      options: [
        [
          {text: "True", letter: "A", index: 0, lockedIn: false},
          {text: "False", letter: "B", index: 1, lockedIn: false },
        ],
        [
          {text: "True", letter: "A", index: 0, lockedIn: false},
          {text: "False", letter: "B", index: 1, lockedIn: false },
          {text: "None of the above", letter: "C", index: 2, lockedIn: false }, //LOL
        ],
        [
          {text: "x is true, y is false", letter: "A", index: 0, lockedIn: false},
          {text: "x is true, y is true", letter: "B", index: 1, lockedIn: false },
          {text:"x is false, y is false", letter: "C", index: 2, lockedIn: false},
          {text:"x is false, y is true", letter: "D", index: 3, lockedIn: false }
        ]
      ],
      answers: [0, 0, 1],
    },
    answer: -1,
  },
  methods: {
    selectOption: function(e) {
      console.log(e);
      let n = Number.parseInt(e.target.dataset['index']);
      if(n === this.current_selected) {
        this.toConfirm = false;
        this.current_answers[this.current_selected].lockedIn = false;
        this.current_selected = -1;
      } else {
        if(this.current_selected >= 0) {
          this.current_answers[this.current_selected].lockedIn = false;
        }
        this.toConfirm = true;
        this.current_selected = n;
        console.log(n);
        this.current_answers[n].lockedIn = true;
      }
    },
    lockIn: function() {
      console.log(this.answer);
      if(this.current_selected >= 0) {
        if(this.toConfirm && !this.goNext) {
          //Show explanation and answer
          if(this.current_selected === this.answer) {
            this.correct_answers++;
          }
          this.goNext = true;
        } else if(this.toConfirm && this.goNext) {
          this.current_answers[this.current_selected].lockedIn = false;
          this.current_question_index++;
          this.current_selected = -1;
          if(this.current_question_index < this.quiz.questions.length) {
            this.answer = this.quiz.answers[this.current_question_index];
            this.current_answers = this.quiz.options[this.current_question_index];
            this.current_question = this.quiz.questions[this.current_question_index];
          }
          //Move to the next question
          this.toConfirm = false;
          this.goNext = false;
        }
      } else {

        this.current_question_index++;
        if(this.current_question_index < this.quiz.questions.length) {
          this.answer = this.quiz.answers[this.current_question_index];
          this.current_answers = this.quiz.options[this.current_question_index];
          this.current_question = this.quiz.questions[this.current_question_index];
        }
        this.toConfirm = false;
        this.goNext = false;
      }
    }
  }
});

v.answer = v.quiz.answers[0];
