var v = new Vue({
  el: "#quiz",
  data: {
    toConfirm: false,
    goNext: false,
    correct_answers: 0,
    current_question_index: 0,
    current_selected: -1,
    current_question: { text: "What function would you use to output to standard output?"},
    current_answers: [ {text: "console.log", letter: "A", index: 0, lockedIn: false}, { text: "print \"standard output\"", letter: "B", index: 1, lockedIn: false },
    { text:"console.err", letter: "C", index: 2, lockedIn: false}, {text:"printf", letter: "D", index: 3, lockedIn: false }],
    quiz: {
      questions: [{ text: "What function would you use to output to standard output?"}],
      explanations: ["For outputting to standard out in javascript, we use console.log"],
      options: [[]],
      answers: [0]
    },
    answer: -1,
  },
  methods: {
    selectOption: function(e) {
      console.log(e);
      let n = Number.parseInt(e.target.dataset['index']);
      if(n === this.current_selected) {
        this.toConfirm = false;
        this.current_answers[this.current_selected].lockedIn = false;
        this.current_selected = -1;
      } else {
        if(this.current_selected >= 0) {
          this.current_answers[this.current_selected].lockedIn = false;
        }
        this.toConfirm = true;
        this.current_selected = n;
        console.log(n);
        this.current_answers[n].lockedIn = true;
      }
    },
    lockIn: function() {
      console.log(this.answer);
      if(this.current_selected >= 0) {
        if(this.toConfirm && !this.goNext) {
          //Show explanation and answer
          if(this.current_selected === this.answer) {
            this.correct_answers++;
          }
          this.goNext = true;
        } else if(this.toConfirm && this.goNext) {
          this.current_answers[this.current_selected].lockedIn = false;
          this.current_question_index++;
          this.current_selected = -1;
          if(this.current_question_index < this.quiz.questions.length) {
            this.answer = this.quiz.answers[this.current_question_index];
            this.current_answers = this.quiz.options[this.current_question_index];
            this.current_question = this.quiz.questions[this.current_question_index];
          }
          //Move to the next question
          this.toConfirm = false;
          this.goNext = false;
        }
      } else {

        this.current_question_index++;
        if(this.current_question_index < this.quiz.questions.length) {
          this.answer = this.quiz.answers[this.current_question_index];
          this.current_answers = this.quiz.options[this.current_question_index];
          this.current_question = this.quiz.questions[this.current_question_index];
        }
        this.toConfirm = false;
        this.goNext = false;
      }
    }
  }
});

v.answer = v.quiz.answers[0];
