

var editor = new Vue({
  el: '#cc_editor',
  data: {
    completed: false,
    addFileData: {
      dialogOpen: false,
      path: '',
    },
    challenge: {
      heading: 'For Loop Challenge',
      body: ['You are tasked will writing that will loop from 0 to 10 and sum all values of i to a variable.', 'Your program should output 45'],
      id: 0,
    },
    files: [
      { name: 'index.js', isFolder: false, parent: -1, collapsed: false,
      childFiles: null, openFileIndex: -1, content:"", index: 0 },
    ],
    testCases: [
      { test: "for loop output test", output: '45\n' }
    ],
    testRun: [

    ],
    editorContext: {
      lines: 0,
      content: '',
      file: null,
      undo: [],
      redo: [],
    },
    openFiles: [],
    marker: {
      attemptMade: false
    }
  },
  methods: {
    openFile: function(e) {
      let n = Number.parseInt(e.target.dataset.findex);
      if(e.target.dataset.findex) {
        if(this.files[n]) {
          if(!this.files[n].isFolder) {
            //It is a file
            console.log("Opened File");
            let found = false;
            for(let i = 0; i < this.openFiles.length; i++) {
              if(this.files[n] === this.openFiles[i]) {
                found = true;
              }
            }
            if(!found) {
              let oIndex = this.openFiles.length;
              this.openFiles.push(this.files[n]);
              this.files[n].openFileIndex = oIndex;
              this.editorContext.file = this.files[n];
              this.editorContext.content = this.files[n].content;
              //this.focusFile()
            } else {
              //Just create focus

            }
          }
        }
      }
    },
    closeFile: function(e) {
      if(e.target.dataset.index && e.target.dataset.oindex) {
        let o = Number.parseInt(e.target.dataset.oindex);
        let i = Number.parseInt(e.target.dataset.index);
        if(this.files[i]) {
          if(this.files[i] === this.editorContext.file) {
            this.editorContext.file = null;
            this.files[i].content = this.editorContext.content;
            this.editorContext.content = '';
            this.files[i].openFileIndex = -1;
            this.openFiles.splice(o, 1);
          }
            //It is a file
            //this.openFiles.splice(this.files[e.target.dataset.findex]);
        }
      }
    },
    addFile: function(e) {
      this.addFileData.dialogOpen = true;
    },
    cancelFile: function(e) {
      this.addFileData.path = '';
      this.addFileData.dialogOpen = false;
    },
    addConfirmFile: function(e) {
      /*
      dialogOpen: true,
      path: '',
      name: '',
      parent: -1,
      isFolder: false,
      childFiles: null,
      collapsed: false,
      */

      let pathSpl = this.addFileData.path.split("/");
      let depth = pathSpl.length-1;
      let set = this.files;
      let parentIndex = -1;

      if(depth > 0) {
        for(let i = 0; i < depth; i++) {
          for(let j = 0; j < set.length; j++) {
            if(pathSpl[i] === set[j].name) {
              parentIndex = j;
              set = set[j].childFiles;
              break;
            }
          }
        }
      }

      set.push(
        { name: pathSpl[pathSpl.length-1], isFolder: false, index: set.length,
          parent: parentIndex, content: '', childFiles: [], collapsed: false }
      );
      this.addFileData.path = '';
      this.addFileData.dialogOpen = false;
    },
    undo: function(e) {
      let con = this.editorContext.undo.pop();
      console.log(con);
      this.editorContext.redo.push(this.editorContext.content);
      this.editorContext.content = this.editorContext.undo.pop();
    },
    redo: function(e) {
      let con = this.editorContext.redo.pop();
      this.editorContext.undo.push(this.editorContext.content);
    },
    markSolution: function(e) {
      this.testRun = []; //Dispose of old results
      console.log("Marking solution");

      //Tricky last bit
      //Just eval prior, only use index.js
      let out = eval('' + this.editorContext.content + ''); //Super dangerous
      //Rewrite to use backend instead and encapsulate execution in sandbox
      console.log("out " + out);
      let allCorrect = true;
      for(let k in this.testCases) {
        if(out === this.testCases[k].output) {
          this.testRun.push({ name: this.testCases[k].test, passed: true });
        } else {
          this.testRun.push({ name: this.testCases[k].test, passed: false });
          allCorrect = false;
        }
      }
      this.completed = allCorrect;
      this.marker.attemptMade = true;

    },
    toggleFolder: function(e) {
      let n = Number.parseInt(e.target.dataset.findex);
      if(e.target.dataset.findex) {
        if(this.files[n]) {
          if(this.files[n].isFolder) {
            this.files[n].collapsed = !this.files[n].collapsed;
            console.log(this.files[n].collapsed);
          }
        }
      }
    },
    parse: function(e) {
      try {
        eval(this.editorContext.content);
      } catch(e) {
        console.log(e);
      }
    },
    focusFile: function(e) {
      this.editorContext.file = this.openFiles[0];

    },
    detectNewLine: function(e) {
      if(e.keyCode === 13) {
        this.editorContext.lines++;
      } else if(e.keyCode === 8) {
        if(this.editorContext.lines > 0) {
          this.editorContext.lines = e.target.value.split("\n").length-1;
          if(e.target.value[e.target.selectionStart-1] === '\n') {
            this.editorContext.lines--;
          }
        }

      }
    }
  }
});


let oIndex = 0;
editor.openFiles.push(editor.files[0]);
editor.files[0].openFileIndex = oIndex;
editor.editorContext.file = editor.files[0];
editor.editorContext.content = editor.files[0].content;

//For recording undos
window.setInterval(function() {
  //Process current state of context and file
  if(editor.editorContext.file) {
    editor.editorContext.file.content = editor.editorContext.content;
  }
  //update undo -Swap to hash system in future
  if(editor.editorContext.undo[editor.editorContext.undo.length-1] !==
    editor.editorContext.content) {
    if(editor.editorContext.undo.length >= 30) {
      editor.editorContext.undo.shift();
    }
    editor.editorContext.undo.push(editor.editorContext.content);
  }
}, 2000);

//Hack solution to get print out
console.plog = console.log;
console.log = function(v) {
  console.plog(v);
  return v + '\n';
}
