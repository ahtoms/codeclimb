var v = new Vue({
  el: "#quiz",
  data: {
    toConfirm: false,
    goNext: false,
    correct_answers: 0,
    current_question_index: 0,
    current_selected: -1,
    current_question: { text: "Is this a valid declaration of a variable? var x;"},
    current_answers: [
      {text: "True", letter: "A", index: 0, lockedIn: false},
      {text: "False", letter: "B", index: 1, lockedIn: false },
    ],
    quiz: {
      questions: [
        { text: "Is this a valid declaration of a variable? var x;"},
        { text: "Is this a valid initialisation of a variable? var x = 5"},
        { text: "What is the value of x? var x = 10*2;"},
        { text: "What data type is this variable? var x = '3';"},
        { text: "What would this evaluate to? var x = 3 + '3';"},
        { text: "Is the let keyword a valid keyword to declare a variable?"},
        { text: "var y = 10; var x = 25; x = x + y;, what is the value of x?"},
      ],
      explanations: [
        "Yes, declared but not initialised",
        "Yes, x is initialised to the valud 5",
        "20",
        "String type as it has '' around the characters",
        "33, since we are performing a string concatenation",
        "Yes, it was introduced in ES6 and has different scoping rules to var",
        "35"
      ],
      options: [
        [
          {text: "True", letter: "A", index: 0, lockedIn: false},
          {text: "False", letter: "B", index: 1, lockedIn: false },
        ],
        [
          {text: "True", letter: "A", index: 0, lockedIn: false},
          {text: "False", letter: "B", index: 1, lockedIn: false },
        ],
        [
          {text: "10", letter: "A", index: 0, lockedIn: false},
          {text: "1", letter: "B", index: 1, lockedIn: false },
          {text:"20", letter: "C", index: 2, lockedIn: false},
          {text:"2", letter: "D", index: 3, lockedIn: false }
        ],
        [
          {text: "String", letter: "A", index: 0, lockedIn: false},
          {text: "Integer", letter: "B", index: 1, lockedIn: false },
          {text: "Float", letter: "B", index: 1, lockedIn: false },
          {text: "Boolean", letter: "B", index: 1, lockedIn: false },
        ],
        [
          {text: "3", letter: "A", index: 0, lockedIn: false},
          {text: "6", letter: "B", index: 1, lockedIn: false },
          {text: "'33'", letter: "C", index: 2, lockedIn: false},
          {text:"None of the above", letter: "D", index: 3, lockedIn: false }
        ],
        [
          {text: "True", letter: "A", index: 0, lockedIn: false},
          {text: "False", letter: "B", index: 1, lockedIn: false },
        ],
        [
          {text: "25", letter: "A", index: 0, lockedIn: false},
          {text: "35", letter: "B", index: 1, lockedIn: false },
          {text: "15", letter: "C", index: 2, lockedIn: false},
          {text:"10", letter: "D", index: 3, lockedIn: false }
        ],
      ],
      answers: [0, 0, 2, 0, 0, 2, 3],
    },
    answer: -1,
  },
  methods: {
    selectOption: function(e) {
      console.log(e);
      let n = Number.parseInt(e.target.dataset['index']);
      if(n === this.current_selected) {
        this.toConfirm = false;
        this.current_answers[this.current_selected].lockedIn = false;
        this.current_selected = -1;
      } else {
        if(this.current_selected >= 0) {
          this.current_answers[this.current_selected].lockedIn = false;
        }
        this.toConfirm = true;
        this.current_selected = n;
        console.log(n);
        this.current_answers[n].lockedIn = true;
      }
    },
    lockIn: function() {
      console.log(this.answer);
      if(this.current_selected >= 0) {
        if(this.toConfirm && !this.goNext) {
          //Show explanation and answer
          if(this.current_selected === this.answer) {
            this.correct_answers++;
          }
          this.goNext = true;
        } else if(this.toConfirm && this.goNext) {
          this.current_answers[this.current_selected].lockedIn = false;
          this.current_question_index++;
          this.current_selected = -1;
          if(this.current_question_index < this.quiz.questions.length) {
            this.answer = this.quiz.answers[this.current_question_index];
            this.current_answers = this.quiz.options[this.current_question_index];
            this.current_question = this.quiz.questions[this.current_question_index];
          }
          //Move to the next question
          this.toConfirm = false;
          this.goNext = false;
        }
      } else {

        this.current_question_index++;
        if(this.current_question_index < this.quiz.questions.length) {
          this.answer = this.quiz.answers[this.current_question_index];
          this.current_answers = this.quiz.options[this.current_question_index];
          this.current_question = this.quiz.questions[this.current_question_index];
        }
        this.toConfirm = false;
        this.goNext = false;
      }
    }
  }
});

v.answer = v.quiz.answers[0];
