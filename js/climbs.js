var profile = new Vue({
  el:"#climbs_updatable",
  data: {
    sidePanelToggle: true,
    climbs: [
      { x: 0, y: 0, name: 'Javascript Fundamentals', url: 'progress.html'}
    ]
  },
  methods: {
    togglePanel: function(e) {
      let element = document.getElementById('side_planel_id');
      if(element) {

        if(this.sidePanelToggle) {
          element.style.left = '-45%';
        } else {
          element.style.left = '0';
        }
        this.sidePanelToggle = !this.sidePanelToggle;
      }
    }
  }
});
