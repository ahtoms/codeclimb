var v = new Vue({
  el: "#quiz",
  data: {
    toConfirm: false,
    goNext: false,
    correct_answers: 0,
    current_question_index: 0,
    current_selected: -1,
    current_question: { text: "Is this a valid for loop? for(var i = 0; i < 10; i++) {  }"},
    current_answers: [
      {text: "True", letter: "A", index: 0, lockedIn: false},
      {text: "False", letter: "B", index: 1, lockedIn: false },
    ],
    quiz: {
      questions: [
        { text: "Is this a valid for loop? for(var i = 0; i < 10; i++) {  }"},
        { text: "Can you use more than one variable "},
        { text: "How many iterations does this loop go through? for(var i = -5; i < 10; i += 2) {}"},
        { text: "Is this an infinite loop? for(let i = 0; i < 10; i += 0) { }"},
        { text: "What part of the for loop performs the update part? for(var i = 0; i < 10; i++)"},
        { text: "What part of the for loop performs the variable initialisation part? for(var i = 0; i < 10; i++)"},
        { text: "What part of the for loop checks the condition? for(var i = 0; i < 10; i++)"},
      ],
      explanations: [
        "Yes, it is a very basic for loop",
        "Yes, you can use more than one variable in all parts of the for loop, this is typically denoted with ','",
        "5 iterations as we are adding 2 for each iteration",
        "Yep! it does not end because i does not change",
        "i++, is the update component",
        "var i = 0, is the variable initialisation component",
        "i < 10 checks the condition"
      ],
      options: [
        [
          {text: "True", letter: "A", index: 0, lockedIn: false},
          {text: "False", letter: "B", index: 1, lockedIn: false },
        ],
        [
          {text: "True", letter: "A", index: 0, lockedIn: false},
          {text: "False", letter: "B", index: 1, lockedIn: false },
        ],
        [
          {text: "10", letter: "A", index: 0, lockedIn: false},
          {text: "20", letter: "B", index: 1, lockedIn: false },
          {text:"5", letter: "C", index: 2, lockedIn: false},
          {text:"2", letter: "D", index: 3, lockedIn: false }
        ],
        [
          {text: "True", letter: "A", index: 0, lockedIn: false},
          {text: "False", letter: "B", index: 1, lockedIn: false },
        ],
        [
          {text: "i++", letter: "A", index: 0, lockedIn: false},
          {text: "for", letter: "B", index: 1, lockedIn: false },
          {text: "i = 0", letter: "C", index: 2, lockedIn: false},
          {text:"i < 10", letter: "D", index: 3, lockedIn: false }
        ],
        [
          {text: "i++", letter: "A", index: 0, lockedIn: false},
          {text: "for", letter: "B", index: 1, lockedIn: false },
          {text: "i = 0", letter: "C", index: 2, lockedIn: false},
          {text:"i < 10", letter: "D", index: 3, lockedIn: false }
        ],
        [
          {text: "i++", letter: "A", index: 0, lockedIn: false},
          {text: "for", letter: "B", index: 1, lockedIn: false },
          {text: "i = 0", letter: "C", index: 2, lockedIn: false},
          {text:"i < 10", letter: "D", index: 3, lockedIn: false }
        ],
      ],
      answers: [0, 0, 2, 0, 0, 2, 3],
    },
    answer: -1,
  },
  methods: {
    selectOption: function(e) {
      console.log(e);
      let n = Number.parseInt(e.target.dataset['index']);
      if(n === this.current_selected) {
        this.toConfirm = false;
        this.current_answers[this.current_selected].lockedIn = false;
        this.current_selected = -1;
      } else {
        if(this.current_selected >= 0) {
          this.current_answers[this.current_selected].lockedIn = false;
        }
        this.toConfirm = true;
        this.current_selected = n;
        console.log(n);
        this.current_answers[n].lockedIn = true;
      }
    },
    lockIn: function() {
      console.log(this.answer);
      if(this.current_selected >= 0) {
        if(this.toConfirm && !this.goNext) {
          //Show explanation and answer
          if(this.current_selected === this.answer) {
            this.correct_answers++;
          }
          this.goNext = true;
        } else if(this.toConfirm && this.goNext) {
          this.current_answers[this.current_selected].lockedIn = false;
          this.current_question_index++;
          this.current_selected = -1;
          if(this.current_question_index < this.quiz.questions.length) {
            this.answer = this.quiz.answers[this.current_question_index];
            this.current_answers = this.quiz.options[this.current_question_index];
            this.current_question = this.quiz.questions[this.current_question_index];
          }
          //Move to the next question
          this.toConfirm = false;
          this.goNext = false;
        }
      } else {

        this.current_question_index++;
        if(this.current_question_index < this.quiz.questions.length) {
          this.answer = this.quiz.answers[this.current_question_index];
          this.current_answers = this.quiz.options[this.current_question_index];
          this.current_question = this.quiz.questions[this.current_question_index];
        }
        this.toConfirm = false;
        this.goNext = false;
      }
    }
  }
});

v.answer = v.quiz.answers[0];
